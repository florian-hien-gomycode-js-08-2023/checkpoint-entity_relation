Vous décidez de rejoindre une très grande chaîne de salles de sport connue. A votre arrivée, vous vous présentez à la réception. Vous trouvez le propriétaire en train de se disputer avec un membre qui souhaite assister à une session à laquelle il n'est pas inscrit. Votre tour arrive enfin, le propriétaire s'excuse de vous avoir fait attendre si longtemps. Il explique que son employé, chargé des inscriptions, s'est trompé en choisissant la séance des membres.

Vous êtes surpris qu'un gymnase aussi reconnu utilise encore les cartes pour gérer son grand nombre de clients. Vous parlez donc au propriétaire pour résoudre ce problème. Intéressé par vos connaissances, il vous demande de lui trouver une solution, en échange il vous proposera une inscription gratuite de 3 mois.  

Le propriétaire vous informe que :

 il possède plusieurs gymnases qui se distinguent par leurs noms, adresses et numéros de téléphone. 
Les membres peuvent s'inscrire dans l'un de ces gymnases, ils doivent donc fournir les informations suivantes : identifiant unique, nom, prénom, adresse, date de naissance et sexe. 
Chaque séance est caractérisée par un type de sport et un horaire et peut accueillir un maximum de 20 adhérents. 
Les séances sont animées par deux coachs maximum qui ont un nom, prénom, âge et spécialité.
